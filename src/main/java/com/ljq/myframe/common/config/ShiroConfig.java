package com.ljq.myframe.common.config;


;
import com.ljq.myframe.common.shiro.MyShiroRealm;
import com.ljq.myframe.common.shiro.ShiroFormAuthenticationFilter;
import com.ljq.myframe.common.shiro.ShiroSessionManager;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.Filter;

import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.session.mgt.SessionManager;

import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ShiroConfig {


  @Bean
  public ShiroFilterFactoryBean shiroFilter(){
    System.out.println("******正在加载shiro配置******");
    ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
    shiroFilterFactoryBean.setSecurityManager(securityManager());

    Map<String, Filter> filtersMap = new LinkedHashMap<>();
    filtersMap.put("shiroAuthc", new ShiroFormAuthenticationFilter());
    shiroFilterFactoryBean.setFilters(filtersMap);


    //设置拦截器
    Map<String, String> map = new LinkedHashMap<>();

    // 游客权限
    map.put("/guest/**", "anon");
    map.put("/user/**", "roles[user]");
    map.put("/admin/**", "roles[admin]");
    map.put("/**", "authc");


    //设置无权限是跳转的URL
    shiroFilterFactoryBean.setUnauthorizedUrl("/403");
    shiroFilterFactoryBean.setLoginUrl("/login");
    shiroFilterFactoryBean.setFilterChainDefinitionMap(map);
    return shiroFilterFactoryBean;
  }
  @Bean
  public SecurityManager securityManager(){
    DefaultSecurityManager securityManager = new DefaultWebSecurityManager();
    securityManager.setRealm(myShiroRealm());
    //自定义session管理
//    securityManager.setSessionManager(sessionManager());
    //自定义缓存实现
    //securityManager.setCacheManager(cacheManager());
    return securityManager;
  }
  @Bean
  public SessionManager sessionManager() {
   ShiroSessionManager shiroSessionManager = new ShiroSessionManager();
    //实现sessionManager
    return shiroSessionManager;
  }

  @Bean
  public Realm myShiroRealm() {
    //实现自己的realm配置
    MyShiroRealm myShiroRealm = new MyShiroRealm();
    return myShiroRealm;
  }
}

