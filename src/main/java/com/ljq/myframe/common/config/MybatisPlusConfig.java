package com.ljq.myframe.common.config;


import com.baomidou.mybatisplus.entity.GlobalConfiguration;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MybatisPlusConfig {
  @Bean
  public PaginationInterceptor paginationInterceptor() {
    PaginationInterceptor page = new PaginationInterceptor();
    page.setDialectType("mysql");
    return page;
  }
//  @Bean
//  public GlobalConfiguration config(){
//    GlobalConfiguration globalConfiguration = new GlobalConfiguration();
//    globalConfiguration.setIdType();
//  }
}
