package com.ljq.myframe.common;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("guest")
public class GuestTestContoller {

  @RequestMapping("page")
  @ResponseBody
  public String page(){
    return "this is guest page";
  }
}
