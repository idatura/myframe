package com.ljq.myframe.common;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("admin")
public class AdminTestContoller {

  @ResponseBody
  @RequestMapping("page")
  public String page(){
    return "this is admin page";
  }
}
