package com.ljq.myframe.common.shiro;



import java.io.Serializable;
import java.util.logging.Logger;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.web.servlet.ShiroHttpServletRequest;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.util.WebUtils;

import org.springframework.util.StringUtils;

public class ShiroSessionManager extends DefaultWebSessionManager {

  @Override
  protected Serializable getSessionId(ServletRequest request, ServletResponse response) {
    //验证请求头是否携带token
    HttpServletRequest httpServletRequest = WebUtils.toHttp(request);
    String token = httpServletRequest.getHeader("token");
    System.out.println("token:" + token);
//    if (!StringUtils.isEmpty(token)) {
      request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_SOURCE, "token");
      request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID, token);
      request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_IS_VALID, Boolean.TRUE);
      return token;
//    } else {
//      return super.getSessionId(request, response);
//    }
  }
}
