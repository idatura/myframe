package com.ljq.myframe.common;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class TestController {
@RequestMapping("/403")
public void error(){
  System.out.println("this is 403 page");
}
@ResponseBody
@RequestMapping("/login")
public String login(){
  System.out.println("this is login page");
  return "this is login page";
}

}
