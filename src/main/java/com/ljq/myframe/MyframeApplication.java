package com.ljq.myframe;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
@MapperScan("com.ljq.myframe.modules.*.mapper")
@ComponentScan("com.ljq")
@SpringBootApplication
public class MyframeApplication {

  public static void main(String[] args) {
    SpringApplication.run(MyframeApplication.class, args);
  }
}
